/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobsheet1_latihan;

/**
 *
 * @author ASUS
 */
public class Hp {
    //Deklarasi
    private String merk,tipe,warna;
    private double harga;
    //Setter
    public void setMerk(String merk){
        this.merk = merk;
    }
    public void  setTipe(String tipe){
        this.tipe = tipe;
    }
    public void setWarna(String warna){
        this.warna = warna;
    }
    public  void setHarga(double harga){
        this.harga = harga;
    }
    //getter
    public String getMerk(){
        return merk;
    }
    public String getTipe(){
        return tipe;
    }
    public String getWarna(){
        return warna;
    }
    public  double getHarga(){
        return harga;
    }
    //Method Tambahan
    public  double  HargaDiskon(){
        double diskon = 0.1 * getHarga();
        double total =  getHarga() -diskon ;
        return total;
    
    }
    public void  keterangan(){
        System.out.println("Harga HP Sesudah diskon (10%) = Rp "+HargaDiskon());
    }

}
