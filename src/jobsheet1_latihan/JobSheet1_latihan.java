/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jobsheet1_latihan;

import java.io.*;

/**
 *
 * @author ASUS
 */
public class JobSheet1_latihan {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        //instance of class
        BufferedReader scan = new BufferedReader(new InputStreamReader(System.in));
        Hp hp = new Hp();
        //input
        System.out.print("Masukkan Merk Hp : ");
       String merkhp = scan.readLine();
       System.out.print("Masukkan Tipe Hp : ");
       String tipe = scan.readLine();
       System.out.print("Masukkan Warna Hp : ");
       String warna = scan.readLine();
       System.out.print("Masukkan harga Hp : ");
       double harga = Double.parseDouble(scan.readLine());
       hp.setMerk(merkhp);
       hp.setTipe(tipe);
       hp.setWarna(warna);
       hp.setHarga(harga);
//output
        System.out.println("======================================");
        System.out.println("DAFTAR HARGA PONSEL DAN SPESIFIKASINYA");
        System.out.println("======================================");
        System.out.println("Merk Hp : "+hp.getMerk());
        System.out.println("Tipe Hp : "+hp.getTipe());
        System.out.println("Warga Hp: "+hp.getWarna());
        System.out.println("Harga Sebelum Diskon = Rp "+hp.getHarga());
        hp.keterangan();
    }
    
}

